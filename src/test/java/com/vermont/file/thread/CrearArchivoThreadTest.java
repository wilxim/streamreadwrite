package com.vermont.file.thread;

import com.vermont.file.services.InputOutputFileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class CrearArchivoThreadTest {

    String fileName = "";
    String path = "src/main/resources/static/";
    String name = "inputfile.txt";


    @InjectMocks
    CargarFicheroThread cargarFicheroThread;

    @InjectMocks
    InputOutputFileService inputOutputFileService;

    @Mock
    CrearArchivoThread crearArchivoThread;


    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        fileName = path + name;


    }


}