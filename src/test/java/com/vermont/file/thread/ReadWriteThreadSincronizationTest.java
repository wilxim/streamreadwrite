package com.vermont.file.thread;

import com.vermont.file.services.InputOutputFileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;


public class ReadWriteThreadSincronizationTest extends Thread{

    String fileName = "";
    String path = "src/main/resources/static/";
    String name = "inputfile.txt";
    List<String> data = new ArrayList<>();




    @InjectMocks
    InputOutputFileService inputOutputFileService;


    @InjectMocks
    CrearArchivoThread crearArchivoThread;

    @InjectMocks
    CargarFicheroThread cargarFicheroThread;


    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        fileName = path + name;


    }

    @Test
    public void runThreadCargarFichero() throws Exception {
        cargarFicheroThread = new CargarFicheroThread(inputOutputFileService,fileName,path);
        cargarFicheroThread.run();
        crearArchivoThread = new CrearArchivoThread(inputOutputFileService,fileName,path,cargarFicheroThread.data);
        crearArchivoThread.run();


    }





}