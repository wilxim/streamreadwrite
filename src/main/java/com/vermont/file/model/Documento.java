package com.vermont.file.model;

import java.io.Serializable;

public class Documento implements Serializable {


    private static final long serialVersionUID = -7102423827320549814L;

    private String nombre;
    private String ruta;
    private Long tamaño;
    private byte[] contenido;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public Long getTamaño() {
        return tamaño;
    }

    public void setTamaño(Long tamaño) {
        this.tamaño = tamaño;
    }

    public byte[] getContenido() {
        return contenido;
    }

    public void setContenido(byte[] contenido) {
        this.contenido = contenido;
    }
}
