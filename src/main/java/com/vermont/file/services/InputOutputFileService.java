package com.vermont.file.services;


import com.vermont.file.model.Documento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class InputOutputFileService {


    @Autowired
    private Documento documento;

    /*
    Servicio para la lectura del archivo
     */
    public List<String> readFile( String filename){
        //Existe el archivo
        String chain = "";
        List<String> list = new ArrayList<>();


        try (Stream<String> stream = Files.lines(Paths.get(filename))) {

           list = stream
                   .collect(Collectors.toList());

            System.out.println("Finaliza lectura del archivo");
            return  list;

        } catch (IOException e) {
            e.printStackTrace();

        }

        return list;

    }

    /*
    Servicio de escritura del nuevo Archivo
     */


    public Boolean createFile(String path, String filename, List<String> data )throws IOException {
        String st;
        File newFile = new File(filename );
        newFile.createNewFile();
        Files.write(Paths.get(path +  "outputfile.txt"), data);
        System.out.println("Finaliza la creacion del archivo");
        return true;
        }


    }







