package com.vermont.file.streamreadwrite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamreadwriteApplication {

    public static void main(String[] args) {
        SpringApplication.run(StreamreadwriteApplication.class, args);

        System.out.println("Hello world");

    }


}
