package com.vermont.file.thread;

import com.vermont.file.services.InputOutputFileService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CrearArchivoThread  extends Thread{

    InputOutputFileService inputOutputFileService;
    String filename, path;
    List<String> data = new ArrayList<>();

    public CrearArchivoThread(InputOutputFileService ioFileService, String filename, String path, List<String> data){
        this.inputOutputFileService = ioFileService;
        this.filename = filename;
        this.path = path;
        this.data = data;

    }

    public void run() {
        long startTime = System.currentTimeMillis();
        int i = 0;
        try {
            inputOutputFileService.createFile( path,  filename,  data );
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }

}
